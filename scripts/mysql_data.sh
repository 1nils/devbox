#!/usr/bin/env bash

mysql -u root -proot -e "DROP DATABASE IF EXISTS wordpress;"
mysql -u root -proot -e "DROP USER IF EXISTS 'wordpress'@'%';"

mysql -u root -proot -e "CREATE DATABASE wordpress;"
`mysql -u root -proot -e "\
  CREATE USER 'wordpress'@'%' IDENTIFIED WITH mysql_native_password BY 'wordpress'; \
  GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress'@'%'; \
  FLUSH PRIVILEGES;"`

mysql -u root -proot wordpress < /vagrant/sql/wordpress.sql
