#!/usr/bin/env bash

# Setup wp-config.php
rm /var/www/wp-config.php || true
cp /var/www/wp-config-sample.php /var/www/wp-config.php

sed -i 's/datenbankname_hier_einfuegen/wordpress/' /var/www/wp-config.php
sed -i 's/benutzername_hier_einfuegen/wordpress/' /var/www/wp-config.php
sed -i 's/passwort_hier_einfuegen/wordpress/' /var/www/wp-config.php
sed -i 's/utf8/utf8mb4/' /var/www/wp-config.php
sed -i 's/\x27WP_DEBUG\x27, false/\x27WP_DEBUG\x27, true/' /var/www/wp-config.php
