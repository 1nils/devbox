# Vagrant Box zur Entwicklung

## Setup

### Erwartete Verzeichnisstruktur

Im Projektverzeichnis müssen die Repositories des
coachlight Theme und des trainingssystem Plugin liegen.
Daneben müssen dieses Repo sowie eine entpackte
Wordpress-Installation liegen.

```
.
+-- devbox
+-- wordpress
+-- coachlight.v2
|   +-- coachlight
+-- trainingssystem.v2
|   +-- trainingssystem-plugin
```

### Erster Start

`vagrant up` im devbox Verzeichnis startet die Box.

Beim ersten Start läuft das provisioning von Vagrant. Dabei werden wichtige
Pakete wie php oder apache2 installiert.

Außerdem wird die Datenbank mit einer frischen Installation von
wordpress+trainingssystem aufgesetzt.

`vagrant provision` lässt das provisioning zu einem späteren Zeitpunkt erneut
laufen. Dann werden alle Daten in der DB weggeworfen und ein frischer
Stand neu eingespielt.

## Zugriff auf die Box

Es wird ein Webserver unter [192.168.33.10](http://192.168.33.10/) gestartet.

**(Optional)** Nach Eintrag in die hosts Datei

```
192.168.33.100   coach.local
```

kann auf die lokale Installation über http://coach.local zugegriffen werden.

## Pfade auf dem System

Das Wurzelverzeichnis des apache Webservers liegt in `/var/www/`.
Dort wird die Wordpress-Installation aus dem Projektverzeichnis eingehängt.

## Accounts

### MySQL

`root:root` und `wordpress:wordpress`

### Wordpress

`admin:qwertz`
